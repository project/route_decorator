Route Decorator
---------------

This module allows routes to be decorated, so their output can be altered. It
is mostly a proof-of-concept for getting this into core
(see https://www.drupal.org/project/drupal/issues/3228770),
but is fully-functional.

WARNING: Should this get into core, the API will likely change.

Usage
-----

To decorate a route, declare a decorator in a module's routing.yml file:

```
my_route_decorator:
  # Path has no effect but must be specified, so just use an empty string.
  path: ''
  defaults:
    # The name of the route to decorate.
    _decorates: system.file_system_settings
    # The decorator callback.
    _decorator: '\Drupal\my_module\Controller\Decorator::decorate'
    # If multiple decorators decorate the same route, a priority can be set to
    # control their order. Higher priorities mean that decorators will be
    # applied earlier.
    _decoration_priority: 10
```

Then define a decorator callback. This must have a $build parameter, in any
position. This receives the build array from the original controller or a prior
decorator.

```
public function decorate($build) {
  // Change the $build array and return it.
  return $build;
}
```

The decorator callback will have typehinted parameters handled in the same way
as route controllers as specified in
https://www.drupal.org/docs/8/api/routing-system/parameters-in-routes/using-parameters-in-routes.

See the test modules for further examples.
