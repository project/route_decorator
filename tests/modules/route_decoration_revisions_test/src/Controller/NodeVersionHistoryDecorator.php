<?php

namespace Drupal\route_decoration_revisions_test\Controller;

use Drupal\node\NodeInterface;

/**
 * Decorates the node version history route to add the revision title.
 */
class NodeVersionHistoryDecorator {

  /**
   * Callback for the route_decoration_revisions_test.foo route.
   */
  public function decorate($build, NodeInterface $node) {
    $node_storage = \Drupal::service('entity_type.manager')->getStorage('node');

    // Get the revisions.
    // (There is no API for this AFAICT!)
    $result = $node_storage->getQuery()
      ->accessCheck(TRUE)
      ->allRevisions()
      ->condition($node->getEntityType()->getKey('id'), $node->id())
      ->sort($node->getEntityType()->getKey('revision'), 'DESC')
      ->pager(50)
      ->execute();
    $revisions_ids = array_keys($result);
    $revisions = $node_storage->loadMultipleRevisions($revisions_ids);

    // We insert a column into the table after the first one.
    $column_splice_index = 1;

    array_splice($build['node_revisions_table']['#header'], $column_splice_index, 0, [t('Title')]);

    foreach (array_keys($build['node_revisions_table']['#rows']) as $row_index) {
      $revision = array_shift($revisions);

      $title_cell = [
        'data' => [
          '#markup' => $revision->label(),
        ],
      ];

      // Rows in the table have two possible structures.
      if (isset($build['node_revisions_table']['#rows'][$row_index]['data'])) {
        array_splice($build['node_revisions_table']['#rows'][$row_index]['data'], $column_splice_index, 0, [$title_cell]);
      }
      else {
        array_splice($build['node_revisions_table']['#rows'][$row_index], $column_splice_index, 0, [$title_cell]);
      }
    }

    return $build;
  }

}
