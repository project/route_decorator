<?php

namespace Drupal\route_decorator_test\Controller;

/**
 * Controller for the route decorator test.
 *
 * This holds the original route controller and the decorators for simplicity.
 * In a real scenario they would be in different classes.
 */
class DecorationTestController {

  /**
   * Callback for the original route.
   */
  public function content() {
    $build = [
      'markup' => [
        '#markup' => 'Content',
      ],
    ];

    return $build;
  }

  /**
   * Decorator callback.
   */
  public function decoratorOne($build) {
    $build['markup']['#markup'] = 'Decorator one';

    return $build;
  }

  /**
   * Decorator callback.
   */
  public function decoratorTwo($build) {
    $build['markup']['#markup'] = 'Decorator two';

    return $build;
  }

}
