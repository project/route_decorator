<?php

namespace Drupal\Tests\route_decorator\Kernel;

use Drupal\KernelTests\KernelTestBase;

/**
 * Tests decorated routes.
 *
 * @group route_decorator
 */
class RouteDecorationTest extends KernelTestBase {

  /**
   * The modules to enable.
   *
   * @var array
   */
  protected static $modules = [
    'system',
    'user',
    'route_decorator',
    'route_decorator_test',
  ];

  /**
   * Tests a simple decorated route.
   */
  public function testDecoratedRoute() {
    $http_kernel = $this->container->get('http_kernel');

    $request = Request::create('/route_decorator_test');

    $response = $http_kernel->handle($request);
    // @todo assert content.
  }

  // @todo Add test for route_decoration_revisions_test module.
}
