<?php

namespace Drupal\route_decorator\EventSubscriber;

use Drupal\Core\Routing\RouteBuildEvent;
use Drupal\Core\Routing\RoutingEvents;
use Drupal\route_decorator\Controller\DecoratingController;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Alters decorated routes to use our controller.
 *
 * @see \Drupal\route_decorator\Controller\DecoratingController
 */
class DecorationRouteSubscriber implements EventSubscriberInterface {

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events = [];
    $events[RoutingEvents::ALTER][] = ['onRouteAlter', -1024];
    return $events;
  }

  /**
   * Alters decorated routes.
   *
   * @param \Drupal\Core\Routing\RouteBuildEvent $event
   *   The route build event.
   */
  public function onRouteAlter(RouteBuildEvent $event) {
    $collection = $event->getRouteCollection();
    foreach ($collection->all() as $route_name => $route) {
      if ($route->hasDefault('_decorates')) {
        $decorated_route = $collection->get($route->getDefault('_decorates'));

        // Stash the original decorated route if not already done so by a prior
        // decoration.
        if (!$decorated_route->hasDefault('decorated')) {
          $serialized = serialize($decorated_route);
          $decorated_route->setDefault('decorated', $serialized);
        }

        // Switch the $decorated_route's controller to ours.
        $decorated_route->setDefault('_controller', DecoratingController::class . '::decorate');

        // Add the decorator callback to the stack for the route.
        $decorators = $decorated_route->getDefault('decorators') ?? [];

        $decorator = $route->getDefault('_decorator');
        $decoration_priority = $route->getDefault('_decoration_priority') ?? 0;

        // Store the decorators with the callback as key (which should be
        // unique) so they can be sorted in the next step.
        $decorators[$decorator] = $decoration_priority;

        $decorated_route->setDefault('decorators', $decorators);

        // Remove the decorating route declaration. This keeps the routing table
        // clean of routes with dummy paths.
        $collection->remove($route_name);
      }
    }

    // Sort decorators by priority and change the 'decorators' route default to
    // have the decorators by value, once we no longer need the priority values.
    foreach ($collection->all() as $route) {
      if (!$route->hasDefault('decorators')) {
        continue;
      }

      // Sort the decorators in the order they should be applied. Higher
      // priorities go earlier.
      $decorators = $route->getDefault('decorators');
      asort($decorators);
      $decorators = array_reverse($decorators);

      $route->setDefault('decorators', array_keys($decorators));
    }
  }

}
