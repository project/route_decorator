<?php

namespace Drupal\route_decorator\Controller;

use Drupal\Core\Routing\RouteMatchInterface;
use Symfony\Component\HttpFoundation\Request;
use Drupal\Core\Routing\RouteObjectInterface;

/**
 * Replacement controller for routes which are decorated.
 *
 * This calls the decorated route's original controller, then passes the result
 * to the decorating controllers.
 *
 * This has to repeat a lot of work that happens in core's Routing component.
 * If this functionality were in core, this would all be vastly simplified as
 * the Router class could handle decoration.
 */
class DecoratingController {

  /**
   * Route callback for decorated routes.
   *
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   The route match.
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The request.
   */
  public function decorate(RouteMatchInterface $route_match, Request $request) {
    // Get the route's original definition so we can get its original
    // controller's response.
    $route = $route_match->getRouteObject();
    $original_route = unserialize($route->getDefault('decorated'));

    // Apply route enhancers to the route's defaults.
    $defaults = $original_route->getDefaults();
    $defaults[RouteObjectInterface::ROUTE_OBJECT] = $original_route;
    $defaults = \Drupal::service('route_decorator.route_enhancement')->applyRouteEnhancers($defaults, $request);

    // Get the controller callable from the definition.
    $original_controller = \Drupal::service('controller_resolver')->getControllerFromDefinition($defaults['_controller']);

    // Resolve controller arguments.
    $this->argumentResolver = \Drupal::service('http_kernel.controller.argument_resolver');
    $arguments = $this->argumentResolver->getArguments($request, $original_controller);

    // Call the original controller.
    $build = $original_controller(...$arguments);

    // Work over the list of decorator callbacks, passing the response to each
    // one.
    $decorators = $route->getDefault('decorators');
    foreach ($decorators as $decorator) {
      // Get the callable from the definition.
      $decorator = \Drupal::service('controller_resolver')->getControllerFromDefinition($decorator);

      // Argument resolvers only work with the request, so to pass the response
      // to the decorator and also use argument resolution, we need to stick the
      // build array into the request as an attribute for the argument resolver
      // to find. This allows decorating controllers to specify their $build
      // parameters in any position.
      $request->attributes->set('build', $build);

      // Resolve decorator arguments.
      $arguments = $this->argumentResolver->getArguments($request, $decorator);

      $build = $decorator(...$arguments);
    }

    return $build;
  }

}
